package alumnes;

public class Alumnes {
	
	// TODO: Omplir aquesta classe amb la corresponent informacio
	
	// Nom i cognoms de l'alumne que deposita la pr�ctica
	public static final String AlumnePrimer_CognomPrimer 	= "Lozano";
	public static final String AlumnePrimer_CognomSegon 	= "Gomez";
	public static final String AlumnePrimer_Nom 			= "Ruben";
	public static final String AlumnePrimer_NIU 			= "1217003";

	// Company de l'anterior
	public static boolean AlumnePrimer_vaSol 		= false;
	public static final String Company_CognomPrimer = "Boigues";
	public static final String Company_CognomSegon 	= "Lopez";
	public static final String Company_Nom 			= "Arnau";
	public static final String Company_NIU 			= "1270636";

	
	
	
	
	
	// Do not modify this one
	public String toString(){
		String s = "<alumnes>";
		s += "<alumne><niu>" + AlumnePrimer_NIU.trim() + "</niu><name><![CDATA[ " + AlumnePrimer_CognomPrimer.trim() + " " + AlumnePrimer_CognomSegon.trim() + ", " + AlumnePrimer_Nom.trim() + " ]]></name></alumne>";
		if(!AlumnePrimer_vaSol){
			s += "<alumne><niu>" + Company_NIU.trim() + "</niu><name><![CDATA[ " + Company_CognomPrimer.trim() + " " + Company_CognomSegon.trim() + ", " + Company_Nom.trim() + " ]]></name></alumne>";
		}
		s += "</alumnes>";
		return s;
	}
}
