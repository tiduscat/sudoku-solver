package SortAlgorithms;


/** Clase que implementa l'algorisme de recerca per backtracking en el cas de solucionar un Sudoku*/
public class Backtracking extends SortAlgorithm {
	int [][] Tauler1;
	
	public void backtracking( int fila, int columna,int[][] Tauler) throws Exception
	{
		if(fila != 9)
	    {
			if( Tauler[fila][columna] != 0 )
		    {
				if (columna == 8) backtracking( fila + 1, 0,Tauler) ;
	          	else backtracking( fila, columna + 1,Tauler) ;
		    }
		    else
		    {
		    	for( int i = 1; i < 10; i++ )
		        {
		    		comparisons++;
		            if( checkRow(fila,i,Tauler) && checkCol(columna,i,Tauler) && checkBox(fila,columna,i,Tauler) )
		            {
		            	Tauler[fila][columna] = i ;
		            	mostra();
		            	if (columna == 8) backtracking( fila + 1, 0,Tauler) ;
		            	else backtracking( fila, columna + 1,Tauler) ;
		            }
		         }
		         Tauler[fila][columna] = 0;
		         mostra() ;
		      }
	     }
	     else throw new Exception ("FINAL");
	}
		 
	 
	/** L'heu de cridar cada cop que avalueu un nou node del arbre, perque mostri en pantalla el tauler*/ 
	public void mostra(){
			int cnt=0; for (int i=0;i<9;i++){for (int j=0;j<9;j++){arrayData[cnt]=Tauler1[i][j];cnt++;};} updateVisual();
	}
 
	 /** Comprova si num es valid en la fila row */
	 public boolean checkRow( int row, int num,int[][] Tauler )
	   {
	      for( int col = 0; col < 9; col++ )
	         if( Tauler[row][col] == num )
	            return false ;

	      return true ;
	   }

	 /** Comprova si num es valid en la columna col*/
	   protected boolean checkCol( int col, int num,int[][] Tauler )
	   {
	      for( int row = 0; row < 9; row++ )
	         if( Tauler[row][col] == num )
	            return false ;

	      return true ;
	   }

	   /** Comprova si num es valid en la submatriu 3x3 que conte la columna col i fila row*/
	   protected boolean checkBox( int row, int col, int num,int[][] Tauler  )
	   {
	      row = (row / 3) * 3 ;
	      col = (col / 3) * 3 ;

	      for( int r = 0; r < 3; r++ )
	         for( int c = 0; c < 3; c++ )
	         if( Tauler[row+r][col+c] == num )
	            return false ;

	      return true ;
	   }
  
	   public void sort() {
			Tauler1=new int [9][9];
			int cnt=0; for (int i=0;i<9;i++){for (int j=0;j<9;j++){Tauler1[i][j]=arrayData[cnt]; cnt++;}}
			try {
				backtracking(0,0,Tauler1);
				//backtracking(0,0,Tauler1);
			} catch (Exception e) {			
			}
			mostra();		
	   }
}
