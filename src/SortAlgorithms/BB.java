package SortAlgorithms;

import java.util.Stack;

public class BB extends SortAlgorithm {
	
	public class Node
	{
		public int [][] tauler = new int[9][9];
		public int x;
		public int y;
		public int n;
		
		public void imprimir_matriu ()
		{
			for (int i = 0; i < 9; i++)
			{
				for(int j = 0; j < 9; j++)
				{
					System.out.print(tauler[i][j]);
				}
				System.out.println("");
			}
			System.out.println("");
			System.out.println("");
			System.out.println("");
		}
		
		public Node(int[][] tauler_n, int x_n, int y_n,int n_n)
		{
			if(n_n != -1)
			{
				for (int i = 0; i < 9; i++)
				{
					for (int j = 0; j < 9; j++)
					{
						tauler[i][j] = tauler_n[i][j];
					}
				}
				x = x_n;
				y = y_n;
				n = n_n;
				tauler[x][y] = n;
			}
			else
			{
				x = 0;
				y = 0;
				n = 0;
			}
		}
		
		public void imprimir_x ()
		{
			System.out.println(x);
		}
		
		public void imprimir_y ()
		{
			System.out.println(y);
		}
		
		public void imprimir_n ()
		{
			System.out.println(n);
		}
	}
	
	int [][] Tauler1;
	
	public void branchbound(Stack<Node> pila)  throws Exception{	
		while(!pila.empty())
		{
			Node auxi = new Node(Tauler1, 0,0,-1);
			auxi = pila.pop();
			int matriu[][] = new int [9][9];
			matriu = auxi.tauler;
	        while (matriu[auxi.x][auxi.y] != 0)
	        {
	        	if(auxi.y != 8)
	        	{
	        		auxi.y = auxi.y +1;
	        	}
	        	else
	        	{
	        		auxi.x = auxi.x + 1;
	        		auxi.y = 0;
	        	}
	        	if(auxi.x == 9) 
	        	{
	        		Tauler1 = auxi.tauler;
	        		mostra();
	        	}
	        }
	        if(auxi.x != 9)
	        {
				for (int i = 1; i < 10; i++)
				{
					if (checkRow( auxi.x, i, auxi.tauler)&& checkCol( auxi.y, i, auxi.tauler) && checkBox( auxi.x, auxi.y, i, auxi.tauler))
					{
						comparisons++;
						pila.push(new Node(matriu, auxi.x, auxi.y,i));
						
					}
				}
	        }
		}
	}
	 
	public void mostra(){
		int cnt=0; for (int i=0;i<9;i++){for (int j=0;j<9;j++){arrayData[cnt]=Tauler1[i][j];cnt++;};} updateVisual();
	}
	
	public boolean checkRow( int row, int num,int[][] Tauler )
	{
		for( int col = 0; col < 9; col++ )
			if( Tauler[row][col] == num )return false ;
			return true ;
	}
	
	protected boolean checkCol( int col, int num,int[][] Tauler )
	{
		for( int row = 0; row < 9; row++ )
			if( Tauler[row][col] == num ) return false ;
			return true ;
	}
	
	protected boolean checkBox( int row, int col, int num,int[][] Tauler )
	{
		row = (row / 3) * 3 ;
		col = (col / 3) * 3 ;

		for( int r = 0; r < 3; r++ )
			for( int c = 0; c < 3; c++ )
				if( Tauler[row+r][col+c] == num ) return false ;

		return true ;
	}
	
	public void sort() {
		Tauler1=new int [9][9];
		int cnt=0; for (int i=0;i<9;i++){for (int j=0;j<9;j++){Tauler1[i][j]=arrayData[cnt]; cnt++;}}
		
		try {
			Node aux = new Node(Tauler1,0,0,Tauler1[0][0]);
			Stack<Node> pila = new Stack<Node>();
			pila.push(aux);
			branchbound(pila);
		} catch (Exception e) {			
		}
		mostra();		
   }
}
